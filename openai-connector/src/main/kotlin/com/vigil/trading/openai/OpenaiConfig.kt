package com.vigil.trading.openai

import com.vigil.trading.openai.clients.OpenaiChatClient
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.support.WebClientAdapter
import org.springframework.web.service.invoker.HttpServiceProxyFactory

@EnableConfigurationProperties(OpenaiProperties::class)
@Configuration
open class OpenaiConfig(private val openaiProperties: OpenaiProperties) {
    @Bean
    open fun openAiClient(): OpenaiChatClient {
        val webClient = WebClient.builder()
            .baseUrl("https://api.openai.com")
            .defaultHeader("Authorization", "Bearer ${openaiProperties.apiToken}")
            .build()
        val factory = HttpServiceProxyFactory.builderFor(WebClientAdapter.create(webClient)).build()
        return factory.createClient(OpenaiChatClient::class.java)
    }
}
