package com.vigil.trading.openai.dto

data class OpenaiSentiment(
    val score: Double,
    val reason: String,
)
