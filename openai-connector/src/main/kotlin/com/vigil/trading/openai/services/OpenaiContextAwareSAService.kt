package com.vigil.trading.openai.services

import com.fasterxml.jackson.databind.ObjectMapper
import com.vigil.trading.openai.OpenaiProperties
import com.vigil.trading.openai.clients.OpenaiChatClient
import com.vigil.trading.openai.constant.OpenaiRole
import com.vigil.trading.openai.dto.Message
import com.vigil.trading.openai.dto.OpenaiSentiment
import com.vigil.trading.openai.dto.OpenaiSentimentResponse
import org.springframework.stereotype.Service

@Service
class OpenaiContextAwareSAService(
    openaiProperties: OpenaiProperties,
    openaiChatClient: OpenaiChatClient,
    objectMapper: ObjectMapper
) : AbstractOpenaiSAService(openaiProperties, openaiChatClient, objectMapper) {

    private val sessions = mutableMapOf<String, MutableList<Message>>()

    fun getSentiment(symbol: String, role: OpenaiRole, content: String): OpenaiSentimentResponse {
        val messages = sessions.computeIfAbsent(symbol) { mutableListOf() }
        messages.add(Message(role.value, content))
        val openaiSentimentCompletion = fetchOpenaiSentimentCompletion(messages)
        messages.add(openaiSentimentCompletion.choices.first().message)
        //TODO: refactor, system prompt should be added only once as for simple approach
        return if (role == OpenaiRole.SYSTEM) OpenaiSentimentResponse(
            OpenaiSentiment(0.0, role.value),
            openaiSentimentCompletion.usage.totalTokens
        )
        else OpenaiSentimentResponse(
            toSentiment(openaiSentimentCompletion.choices.first().message.content),
            openaiSentimentCompletion.usage.totalTokens
        )
    }
}
