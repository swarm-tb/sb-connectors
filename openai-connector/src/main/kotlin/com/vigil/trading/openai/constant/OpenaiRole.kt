package com.vigil.trading.openai.constant

enum class OpenaiRole(val value: String) {
    SYSTEM("system"),
    USER("user"),
    ASSISTANT("assistant");
}
