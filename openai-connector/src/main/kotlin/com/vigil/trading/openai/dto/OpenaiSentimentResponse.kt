package com.vigil.trading.openai.dto

data class OpenaiSentimentResponse(
    val sentiment: OpenaiSentiment,
    val tokensUsed: Int
)
