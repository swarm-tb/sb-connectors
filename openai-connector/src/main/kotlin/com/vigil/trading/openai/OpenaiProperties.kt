package com.vigil.trading.openai

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("openai")
data class OpenaiProperties(
    val apiToken: String,
    val model: String = "gpt-4o",
    val maxTokens: Int = 100,
    val temperature: Double = 0.0,
    val sentimentAnalysisPrompt: String
)
