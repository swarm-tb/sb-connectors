package com.vigil.trading.openai.services

import com.fasterxml.jackson.databind.ObjectMapper
import com.vigil.trading.openai.OpenaiProperties
import com.vigil.trading.openai.clients.OpenaiChatClient
import com.vigil.trading.openai.constant.OpenaiRole
import com.vigil.trading.openai.dto.Message
import com.vigil.trading.openai.dto.OpenaiSentimentResponse
import org.springframework.stereotype.Service
import java.util.concurrent.ConcurrentHashMap

@Service
class OpenaiSimpleSAService(
    openaiProperties: OpenaiProperties,
    openaiChatClient: OpenaiChatClient,
    objectMapper: ObjectMapper
) : AbstractOpenaiSAService(openaiProperties, openaiChatClient, objectMapper) {

    private val systemPrompts = ConcurrentHashMap<String, Message>()

    fun getSentiment(symbol: String, content: String): OpenaiSentimentResponse {
        val systemPrompt = systemPrompts.computeIfAbsent(symbol) {
            Message(
                OpenaiRole.SYSTEM.value,
                openaiProperties.sentimentAnalysisPrompt.format(symbol)
            )
        }
        val openaiSentimentCompletion =
            fetchOpenaiSentimentCompletion(listOf(systemPrompt, Message(OpenaiRole.USER.value, content)))
        return OpenaiSentimentResponse(
            toSentiment(openaiSentimentCompletion.choices.first().message.content),
            openaiSentimentCompletion.usage.totalTokens
        )
    }
}
