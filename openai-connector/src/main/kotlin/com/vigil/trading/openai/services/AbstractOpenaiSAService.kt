package com.vigil.trading.openai.services

import com.fasterxml.jackson.databind.ObjectMapper
import com.vigil.trading.openai.OpenaiProperties
import com.vigil.trading.openai.clients.OpenaiChatClient
import com.vigil.trading.openai.dto.Message
import com.vigil.trading.openai.dto.OpenaiChatCompletionRequest
import com.vigil.trading.openai.dto.OpenaiSentiment

abstract class AbstractOpenaiSAService protected constructor(
    protected val openaiProperties: OpenaiProperties,
    private val openaiChatClient: OpenaiChatClient,
    private val objectMapper: ObjectMapper
) {

    protected fun toSentiment(content: String): OpenaiSentiment =
        objectMapper.readValue(content, OpenaiSentiment::class.java)

    protected fun fetchOpenaiSentimentCompletion(messages: List<Message>) =
        openaiChatClient.getCompletion(
            OpenaiChatCompletionRequest(
                openaiProperties.model,
                messages,
                openaiProperties.maxTokens,
                openaiProperties.temperature
            )
        )
}
