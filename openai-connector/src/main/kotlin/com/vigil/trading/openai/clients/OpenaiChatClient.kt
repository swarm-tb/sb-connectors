package com.vigil.trading.openai.clients

import com.vigil.trading.openai.dto.OpenaiChatCompletionRequest
import com.vigil.trading.openai.dto.OpenaiChatCompletionResponse
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.service.annotation.HttpExchange
import org.springframework.web.service.annotation.PostExchange

@HttpExchange("/v1/chat")
interface OpenaiChatClient {

    @PostExchange("/completions")
    fun getCompletion(@RequestBody request: OpenaiChatCompletionRequest): OpenaiChatCompletionResponse
}
