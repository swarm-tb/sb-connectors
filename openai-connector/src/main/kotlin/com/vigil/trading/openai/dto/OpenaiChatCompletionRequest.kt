package com.vigil.trading.openai.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class OpenaiChatCompletionRequest(
    val model: String,
    val messages: List<Message>,
    @JsonProperty("max_tokens")
    val maxTokens: Int,
    val temperature: Double
)

data class Message(
    val role: String,
    val content: String
)

data class TokenUsage(
    @JsonProperty("total_tokens")
    val totalTokens: Int
)

data class OpenaiChatCompletionResponse(
    val id: String,
    val choices: List<Choice>,
    val usage: TokenUsage
)

class Choice {
    lateinit var message: Message
}
