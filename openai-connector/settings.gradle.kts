pluginManagement {
    repositories {
        mavenLocal()
        gradlePluginPortal()
        mavenCentral()
        maven {
            url = uri("${System.getenv("CI_API_V4_URL")}/groups/59954528/-/packages/maven")
        }
    }
}
rootProject.name = "openai-connector"
