package com.vigil.trading.binance.utility;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = BinanceIntervalUtils.class)
class BinanceIntervalUtilsTest {

  @Autowired
  private BinanceIntervalUtils binanceIntervalUtils;

  @Test
  void shouldReturnCorrectBinanceIntervalForDuration() {
    final String expectedInterval = "1h";
    final String actual = binanceIntervalUtils.getInterval(Duration.ofMinutes(60));

    assertEquals(expectedInterval, actual);
  }
}
