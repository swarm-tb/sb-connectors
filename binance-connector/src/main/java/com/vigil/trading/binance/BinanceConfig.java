package com.vigil.trading.binance;

import com.binance.connector.futures.client.impl.UMFuturesClientImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BinanceConfig {

  private final BinanceProperties binanceProperties;

  public BinanceConfig(final BinanceProperties binanceProperties) {
    this.binanceProperties = binanceProperties;
  }

  @Bean
  public UMFuturesClientImpl futuresClient() {
    return new UMFuturesClientImpl(binanceProperties.getApiKey(), binanceProperties.getApiSecret());
  }
}
