package com.vigil.trading.binance.constant;

public enum OrderSide {
  BUY,
  SELL
}
