package com.vigil.trading.binance.constant;

public enum SymbolFilterType {
  PRICE_FILTER,
  LOT_SIZE,
  MARKET_LOT_SIZE,
  MAX_NUM_ORDERS,
  MAX_NUM_ALGO_ORDERS,
  PERCENT_PRICE,
  MIN_NOTIONAL
}
