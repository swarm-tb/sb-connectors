package com.vigil.trading.binance;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vigil.trading.binance.dto.BinanceSymbol;
import java.util.Set;

public interface BinanceSymbolService {

  Set<BinanceSymbol> getAllSymbols() throws JsonProcessingException;
}
