package com.vigil.trading.binance.dto;

import com.vigil.trading.binance.constant.ContractType;
import com.vigil.trading.binance.constant.SymbolStatus;
import com.vigil.trading.binance.dto.filter.SymbolFilter;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BinanceSymbol {

  private String symbol;
  private String baseAsset;
  private String quoteAsset;
  private Integer baseAssetPrecision;
  private SymbolStatus status;
  private ContractType contractType;
  private Set<SymbolFilter> filters;
}
