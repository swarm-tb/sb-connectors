package com.vigil.trading.binance;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vigil.trading.binance.dto.BinanceCandle;
import java.time.Duration;
import java.util.List;

public interface BinanceCandleService {

  List<BinanceCandle> getCandles(
      String symbol, Duration interval, long fromTimestamp, long toTimestamp, int limit
  ) throws JsonProcessingException;
}
