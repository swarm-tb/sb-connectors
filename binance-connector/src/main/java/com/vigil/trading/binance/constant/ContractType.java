package com.vigil.trading.binance.constant;

public enum ContractType {
  PERPETUAL,
  CURRENT_MONTH,
  NEXT_MONTH,
  CURRENT_QUARTER,
  NEXT_QUARTER,
  PERPETUAL_DELIVERING
}
