package com.vigil.trading.binance.impl;

import com.binance.connector.futures.client.FuturesClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vigil.trading.binance.BinanceSymbolService;
import com.vigil.trading.binance.dto.BinanceSymbol;
import com.vigil.trading.binance.dto.ExchangeInfo;
import java.util.Set;
import org.springframework.stereotype.Service;

@Service
public class BinanceSymbolServiceImpl implements BinanceSymbolService {

  private final ObjectMapper objectMapper;
  private final FuturesClient futuresClient;

  public BinanceSymbolServiceImpl(
      final ObjectMapper objectMapper,
      final FuturesClient futuresClient
  ) {
    this.objectMapper = objectMapper;
    this.futuresClient = futuresClient;
  }

  @Override
  public Set<BinanceSymbol> getAllSymbols() throws JsonProcessingException {
    final String exchangeInfoResponse = futuresClient.market().exchangeInfo();
    final ExchangeInfo exchangeInfo = objectMapper.readValue(
        exchangeInfoResponse,
        ExchangeInfo.class
    );
    return exchangeInfo.getSymbols();
  }
}
