package com.vigil.trading.binance.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
import java.time.Instant;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonFormat(shape = JsonFormat.Shape.ARRAY)
public class BinanceCandle {

  private Instant openTime;
  private BigDecimal open;
  private BigDecimal high;
  private BigDecimal low;
  private BigDecimal close;
  private BigDecimal volume;
  private Instant closeTime;
  private BigDecimal quoteAssetVolume;
  private Integer numberOfTrades;
  private BigDecimal takerBuyBaseAssetVolume;
  private BigDecimal takerBuyQuoteAssetVolume;
  private String ignore;
}
