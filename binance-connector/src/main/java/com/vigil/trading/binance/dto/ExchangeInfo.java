package com.vigil.trading.binance.dto;

import java.util.Set;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExchangeInfo {

  private Set<BinanceSymbol> symbols;

}
