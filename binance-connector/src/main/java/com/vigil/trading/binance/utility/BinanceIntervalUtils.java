package com.vigil.trading.binance.utility;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class BinanceIntervalUtils {

  private final Map<Duration, String> durationToIntervalMap;

  public BinanceIntervalUtils() {
    durationToIntervalMap = new HashMap<>();
    durationToIntervalMap.put(Duration.ofMinutes(1), "1m");
    durationToIntervalMap.put(Duration.ofMinutes(3), "3m");
    durationToIntervalMap.put(Duration.ofMinutes(5), "5m");
    durationToIntervalMap.put(Duration.ofMinutes(15), "15m");
    durationToIntervalMap.put(Duration.ofMinutes(30), "30m");
    durationToIntervalMap.put(Duration.ofHours(1), "1h");
    durationToIntervalMap.put(Duration.ofHours(2), "2h");
  }

  public String getInterval(final Duration duration) {
    return durationToIntervalMap.get(duration);
  }
}
