package com.vigil.trading.binance.dto.filter;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.vigil.trading.binance.constant.SymbolFilterType;
import lombok.Getter;
import lombok.Setter;

@JsonTypeInfo(use = Id.NAME, property = "filterType", visible = true)
@JsonSubTypes({
    @Type(value = PriceFilter.class, name = "PRICE_FILTER"),
    @Type(value = LotSizeFilter.class, name = "LOT_SIZE"),
    @Type(value = MarketLotSizeFilter.class, name = "MARKET_LOT_SIZE"),
    @Type(value = MaxNumOrdersFilter.class, name = "MAX_NUM_ORDERS"),
    @Type(value = MaxNumAlgoOrdersFilter.class, name = "MAX_NUM_ALGO_ORDERS"),
    @Type(value = PercentPriceFilter.class, name = "PERCENT_PRICE"),
    @Type(value = MinNotionalFilter.class, name = "MIN_NOTIONAL"),
})
@Getter
@Setter
public abstract class SymbolFilter {

  protected SymbolFilterType filterType;
}
