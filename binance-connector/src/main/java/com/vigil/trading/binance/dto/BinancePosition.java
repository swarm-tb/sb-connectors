package com.vigil.trading.binance.dto;

import com.vigil.trading.binance.PositionSide;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BinancePosition {

  private String symbol;
  private PositionSide positionSide;
  private BigDecimal positionAmt;
  private BigDecimal entryPrice;
  private Integer leverage;

}
