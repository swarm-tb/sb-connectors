package com.vigil.trading.binance.impl;

import com.binance.connector.futures.client.FuturesClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vigil.trading.binance.BinanceCandleService;
import com.vigil.trading.binance.dto.BinanceCandle;
import com.vigil.trading.binance.utility.BinanceIntervalUtils;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class BinanceCandleServiceImpl implements BinanceCandleService {

  private static final String SYMBOL_PARAM = "symbol";
  private static final String INTERVAL_PARAM = "interval";
  private static final String START_TIME_PARAM = "startTime";
  private static final String END_TIME_PARAM = "endTime";
  private static final String LIMIT_PARAM = "limit";

  private final ObjectMapper objectMapper;
  private final BinanceIntervalUtils binanceIntervalUtils;
  private final FuturesClient futuresClient;

  public BinanceCandleServiceImpl(
      final BinanceIntervalUtils binanceIntervalUtils,
      final FuturesClient futuresClient,
      final ObjectMapper objectMapper
  ) {
    this.objectMapper = objectMapper;
    this.binanceIntervalUtils = binanceIntervalUtils;
    this.futuresClient = futuresClient;
  }

  @Override
  public List<BinanceCandle> getCandles(
      final String symbol,
      final Duration interval,
      final long fromTimestamp,
      final long toTimestamp,
      final int limit
  ) throws JsonProcessingException {
    final LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
    parameters.put(SYMBOL_PARAM, symbol);
    parameters.put(INTERVAL_PARAM, binanceIntervalUtils.getInterval(interval));
    parameters.put(START_TIME_PARAM, fromTimestamp);
    parameters.put(END_TIME_PARAM, toTimestamp);
    parameters.put(LIMIT_PARAM, limit);
    final String klinesResponse = futuresClient.market().klines(parameters);
    return objectMapper.readValue(klinesResponse, new TypeReference<>() {});
  }
}
