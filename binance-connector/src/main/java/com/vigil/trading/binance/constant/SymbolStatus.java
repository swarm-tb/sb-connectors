package com.vigil.trading.binance.constant;

public enum SymbolStatus {
  PENDING_TRADING,
  TRADING,
  PRE_DELIVERING,
  DELIVERING,
  DELIVERED,
  PRE_SETTLE,
  SETTLING,
  CLOSE
}
