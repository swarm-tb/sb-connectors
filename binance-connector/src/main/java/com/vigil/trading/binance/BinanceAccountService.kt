package com.vigil.trading.binance

import com.binance.connector.futures.client.impl.UMFuturesClientImpl
import com.fasterxml.jackson.databind.ObjectMapper
import com.vigil.trading.binance.dto.BinanceAccountInfo
import com.vigil.trading.binance.utility.readValueAs
import org.springframework.stereotype.Service

@Service
class BinanceAccountService(
    private val futuresClient: UMFuturesClientImpl,
    private val objectMapper: ObjectMapper
) {
    fun getAccountInfo(): BinanceAccountInfo {
        val accountInfo = futuresClient.account().accountInformation(
                linkedMapOf(RECV_WINDOW_PARAM to RECV_WINDOW_VALUE)
            )
        return objectMapper.readValueAs(accountInfo)
    }

    fun updateInitialPositionLeverage(symbol: String, leverage: Int) {
        val params = linkedMapOf<String, Any>(
            SYMBOL_PARAM to symbol,
            LEVERAGE_PARAM to leverage
        )
        futuresClient.account().changeInitialLeverage(params)
    }

    companion object {
        private const val SYMBOL_PARAM = "symbol"
        private const val LEVERAGE_PARAM = "leverage"
        private const val RECV_WINDOW_PARAM = "recvWindow"
        private const val RECV_WINDOW_VALUE = 10000
    }
}
