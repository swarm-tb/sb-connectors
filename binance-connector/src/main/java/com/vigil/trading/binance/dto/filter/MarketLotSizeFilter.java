package com.vigil.trading.binance.dto.filter;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketLotSizeFilter extends SymbolFilter {

  private BigDecimal minQty;
  private BigDecimal stepSize;
}
