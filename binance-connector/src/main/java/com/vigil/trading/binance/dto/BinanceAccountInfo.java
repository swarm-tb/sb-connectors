package com.vigil.trading.binance.dto;

import java.math.BigDecimal;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BinanceAccountInfo {

  private BigDecimal totalMarginBalance;
  private List<BinancePosition> positions;
}
