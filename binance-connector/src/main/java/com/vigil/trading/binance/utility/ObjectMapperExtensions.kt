package com.vigil.trading.binance.utility

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper

inline fun <reified T> ObjectMapper.readValueAs(value: String): T =
    this.readValue(value, object : TypeReference<T>() {})
