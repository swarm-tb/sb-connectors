package com.vigil.trading.binance.dto.filter;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceFilter extends SymbolFilter {

  private BigDecimal tickSize;
}
