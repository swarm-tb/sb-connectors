package com.vigil.trading.binance;

public enum PositionSide {
  BOTH,
  LONG,
  SHORT
}
