package com.vigil.trading.binance

import com.binance.connector.futures.client.FuturesClient
import com.vigil.trading.binance.constant.OrderSide
import com.vigil.trading.binance.constant.OrderType
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class BinanceOrderService(private val futuresClient: FuturesClient) {
    fun makeMarketBuyOrder(symbol: String, quantity: BigDecimal) {
        val params = linkedMapOf<String, Any>(
            SYMBOL_PARAM to symbol,
            SIDE_PARAM to OrderSide.BUY.name,
            TYPE_PARAM to OrderType.MARKET.name,
            QUANTITY_PARAM to quantity,
            RECV_WINDOW_PARAM to RECV_WINDOW_VALUE
        )
        futuresClient.account().newOrder(params)
    }

    fun makeMarketSellOrder(symbol: String, quantity: BigDecimal) {
        val params = linkedMapOf<String, Any>(
            SYMBOL_PARAM to symbol,
            SIDE_PARAM to OrderSide.SELL.name,
            TYPE_PARAM to OrderType.MARKET.name,
            QUANTITY_PARAM to quantity,
            RECV_WINDOW_PARAM to RECV_WINDOW_VALUE
        )
        futuresClient.account().newOrder(params)
    }

    companion object {
        private const val SYMBOL_PARAM = "symbol"
        private const val SIDE_PARAM = "side"
        private const val TYPE_PARAM = "type"
        private const val QUANTITY_PARAM = "quantity"
        private const val RECV_WINDOW_PARAM = "recvWindow"
        private const val RECV_WINDOW_VALUE = 10000
    }
}
