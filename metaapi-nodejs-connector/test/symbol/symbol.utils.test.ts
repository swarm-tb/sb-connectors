import moment from 'moment';
import { isTradingSession } from '../../src/symbol/symbol.utils';
import { MetatraderSessions } from 'metaapi.cloud-sdk/dist/clients/metaApi/metaApiWebsocket.client'; // Assuming you have Moment.js installed.

describe('isTradingSession', () => {
  const tradeSessions = {
    MONDAY: [{ from: '00:00:00.000', to: '23:59:59.999' }],
    TUESDAY: [{ from: '00:00:00.000', to: '23:59:59.999' }],
    WEDNESDAY: [{ from: '00:00:00.000', to: '23:59:59.999' }],
    THURSDAY: [{ from: '00:00:00.000', to: '23:59:59.999' }],
    FRIDAY: [{ from: '00:00:00.000', to: '23:55:59.999' }],
    SATURDAY: [{ from: '00:05:00.000', to: '21:10:59.999' }],
  } as MetatraderSessions;

  test('should return true if the current time is within a trading session', () => {
    // Set the current time to Saturday 21:00 PM for the test
    const currentTime = moment('2023-07-29T21:00:00.000');
    jest.spyOn(moment, 'now').mockReturnValue(currentTime.valueOf());

    const result = isTradingSession(tradeSessions);
    expect(result).toBe(true);
  });

  test('should return false if the current time is not within any trading session', () => {
    // Set the current time to Saturday 22:00 AM for the test
    const currentTime = moment('2023-07-29T22:00:00.000');
    jest.spyOn(moment, 'now').mockReturnValue(currentTime.valueOf());

    const result = isTradingSession(tradeSessions);
    expect(result).toBe(false);
  });

  test('should return false if the day is not a trading day', () => {
    // Set the current time to Sunday 10:00 AM for the test
    const currentTime = moment('2023-07-30T10:00:00.000');
    jest.spyOn(moment, 'now').mockReturnValue(currentTime.valueOf());

    const result = isTradingSession(tradeSessions);
    expect(result).toBe(false);
  });
});
