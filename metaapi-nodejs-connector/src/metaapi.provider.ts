import { Provider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import MetaApi, { MetatraderAccount, StreamingMetaApiConnectionInstance } from 'metaapi.cloud-sdk';

export interface ConnectionMap {
  [accountId: string]: StreamingMetaApiConnectionInstance;
}

async function ensureDeployment(account: MetatraderAccount) {
  const initialState = account.state;
  const deployedStates = ['DEPLOYING', 'DEPLOYED'];
  if (!deployedStates.includes(initialState)) {
    console.log('Deploying account');
    await account.deploy();
  }
}

async function getStreamingConnectionMap(configService: ConfigService): Promise<ConnectionMap> {
  const metaApi = new MetaApi(configService.get<string>('META_API_ACCESS_TOKEN'));
  const accountIds = configService.get<string>('META_API_ACCOUNT_IDS', '').split(',');
  const connectionMap: ConnectionMap = {};
  for (const accountId of accountIds) {
    connectionMap[accountId] = await getStreamingConnection(metaApi, accountId);
  }
  return connectionMap;
}

async function getStreamingConnection(
  metaApi: MetaApi,
  accountId: string,
): Promise<StreamingMetaApiConnectionInstance> {
  const account = await metaApi.metatraderAccountApi.getAccount(accountId);
  await ensureDeployment(account);
  console.log('Waiting for API server to connect to broker (may take couple of minutes)');
  await account.waitConnected();

  const connection = account.getStreamingConnection();
  await connection.connect();
  // wait until terminal state synchronized to the local state
  console.log('Waiting for SDK to synchronize to terminal state (may take some time depending on your history size)');
  await connection.waitSynchronized();
  return connection;
}

export const MetaApiStreamingConnectionProvider: Provider = {
  provide: 'MetaApiStreamingConnectionMap',
  useFactory: async (configService: ConfigService) => getStreamingConnectionMap(configService),
  inject: [ConfigService],
};
