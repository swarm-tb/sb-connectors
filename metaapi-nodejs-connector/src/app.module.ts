import { Module } from '@nestjs/common';
import { SymbolService } from './symbol/symbol.service';
import { ConfigModule } from '@nestjs/config';
import { MetaApiStreamingConnectionProvider } from './metaapi.provider';
import { SymbolController } from './symbol/symbol.controller';
import { CandleController } from './candle/candle.controller';
import { CandleService } from './candle/candle.service';
import { AccountController } from './account/account.controller';
import { AccountService } from './account/account.service';
import { GrpcReflectionDynamicModule } from './grpc.options';
import { OrderService } from './order/order.service';
import { OrderController } from './order/order.controller';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), GrpcReflectionDynamicModule],
  controllers: [SymbolController, CandleController, AccountController, OrderController],
  providers: [SymbolService, CandleService, AccountService, OrderService, MetaApiStreamingConnectionProvider],
})
export class AppModule {}
