export const stringToEnumValue = <T>(value: string, enumType: T): T[keyof T] => {
  return enumType[value as keyof T] ?? enumType['UNRECOGNIZED'];
};
