import { Inject, Injectable } from '@nestjs/common';
import { MetaApiOrderResponse } from '@swarm-tb/grpc-common/metaapi/meta-api-order';
import { ConnectionMap } from '../metaapi.provider';
import { MetatraderTradeResponse } from 'metaapi.cloud-sdk';

@Injectable()
export class OrderService {
  constructor(
    @Inject('MetaApiStreamingConnectionMap')
    private connectionMap: ConnectionMap,
  ) {}

  async createMarketBuyOrder(
    accountId: string,
    symbol: string,
    size: number,
    stopLossPrice?: number,
  ): Promise<MetaApiOrderResponse> {
    const connection = this.connectionMap[accountId];
    const metatraderTradeResponse = await connection.createMarketBuyOrder(symbol, size, stopLossPrice);
    return this.getMetaApiOrderResponse(metatraderTradeResponse);
  }

  async createMarketSellOrder(
    accountId: string,
    symbol: string,
    size: number,
    stopLossPrice?: number,
  ): Promise<MetaApiOrderResponse> {
    const connection = this.connectionMap[accountId];
    const metatraderTradeResponse = await connection.createMarketSellOrder(symbol, size, stopLossPrice);
    return this.getMetaApiOrderResponse(metatraderTradeResponse);
  }

  async closePosition(accountId: string, symbol: string): Promise<MetaApiOrderResponse> {
    const connection = this.connectionMap[accountId];
    const metatraderTradeResponse = await connection.closePositionsBySymbol(symbol, {});
    return this.getMetaApiOrderResponse(metatraderTradeResponse);
  }

  private getMetaApiOrderResponse(metatraderTradeResponse: MetatraderTradeResponse): MetaApiOrderResponse {
    return {
      orderId: metatraderTradeResponse.orderId,
      positionId: metatraderTradeResponse.positionId,
    };
  }
}
