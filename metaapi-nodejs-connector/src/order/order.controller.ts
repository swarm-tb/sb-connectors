import { Controller } from '@nestjs/common';
import { OrderService } from './order.service';
import {
  MetaApiClosePositionRequest,
  MetaApiOrderRequest,
  MetaApiOrderResponse,
  MetaApiOrderServiceController,
  MetaApiOrderServiceControllerMethods,
} from '@swarm-tb/grpc-common/metaapi/meta-api-order';

@Controller()
@MetaApiOrderServiceControllerMethods()
export class OrderController implements MetaApiOrderServiceController {
  constructor(private readonly orderService: OrderService) {}

  async createMarketBuyOrder(request: MetaApiOrderRequest): Promise<MetaApiOrderResponse> {
    return this.orderService.createMarketBuyOrder(
      request.accountId,
      request.symbol,
      request.volume,
      request.stopLossPrice,
    );
  }

  async createMarketSellOrder(request: MetaApiOrderRequest): Promise<MetaApiOrderResponse> {
    return this.orderService.createMarketSellOrder(
      request.accountId,
      request.symbol,
      request.volume,
      request.stopLossPrice,
    );
  }

  async closePosition(request: MetaApiClosePositionRequest): Promise<MetaApiOrderResponse> {
    return this.orderService.closePosition(request.accountId, request.symbol);
  }
}
