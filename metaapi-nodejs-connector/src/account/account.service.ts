import { Inject, Injectable } from '@nestjs/common';
import { MetaApiAccountInfo, MetaApiPosition, PositionType } from '@swarm-tb/grpc-common/metaapi/meta-api-account';
import { ConnectionMap } from '../metaapi.provider';
import { MetatraderPosition } from 'metaapi.cloud-sdk';
import { stringToEnumValue } from '../common.utils';

@Injectable()
export class AccountService {
  constructor(
    @Inject('MetaApiStreamingConnectionMap')
    private connectionMap: ConnectionMap,
  ) {}

  async getPositions(accountId: string): Promise<MetaApiPosition[]> {
    const connection = this.connectionMap[accountId];
    const metatraderPositions = connection.terminalState.positions;
    return metatraderPositions.map((value) => AccountService.toMetaApiPosition(value));
  }

  async getAccountInfo(accountId: string): Promise<MetaApiAccountInfo> {
    const connection = this.connectionMap[accountId];
    const metatraderAccountInformation = connection.terminalState.accountInformation;
    return {
      balance: metatraderAccountInformation.balance,
      equity: metatraderAccountInformation.equity,
      margin: metatraderAccountInformation.margin,
    };
  }

  private static toMetaApiPosition(metatraderPosition: MetatraderPosition): MetaApiPosition {
    return {
      symbol: metatraderPosition.symbol,
      type: stringToEnumValue(metatraderPosition.type, PositionType),
      volume: metatraderPosition.volume,
      currentPrice: metatraderPosition.currentPrice,
    };
  }
}
