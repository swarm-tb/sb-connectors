import { Controller } from '@nestjs/common';
import { AccountService } from './account.service';
import {
  MetaApiAccountInfo,
  MetaApiAccountInfoRequest,
  MetaApiAccountServiceController,
  MetaApiAccountServiceControllerMethods,
  MetaApiPositionsResponse,
} from '@swarm-tb/grpc-common/metaapi/meta-api-account';

@Controller()
@MetaApiAccountServiceControllerMethods()
export class AccountController implements MetaApiAccountServiceController {
  constructor(private readonly accountService: AccountService) {}

  async getPositions(request: MetaApiAccountInfoRequest): Promise<MetaApiPositionsResponse> {
    return {
      positions: await this.accountService.getPositions(request.accountId),
    };
  }

  async getAccountInfo(request: MetaApiAccountInfoRequest): Promise<MetaApiAccountInfo> {
    return this.accountService.getAccountInfo(request.accountId);
  }
}
