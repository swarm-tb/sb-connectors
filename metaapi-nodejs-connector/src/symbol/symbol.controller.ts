import { Controller } from '@nestjs/common';
import { SymbolService } from './symbol.service';
import {
  MetaApiSymbolServiceController,
  MetaApiSymbolServiceControllerMethods,
  MetaApiSymbolsRequest,
  MetaApiSymbolsResponse,
} from '@swarm-tb/grpc-common/metaapi/meta-api-symbol';

@Controller()
@MetaApiSymbolServiceControllerMethods()
export class SymbolController implements MetaApiSymbolServiceController {
  constructor(private readonly symbolService: SymbolService) {}

  async getAllSymbols(request: MetaApiSymbolsRequest): Promise<MetaApiSymbolsResponse> {
    return {
      symbolsInfo: await this.symbolService.getSymbolsInfo(request.accountId),
    };
  }
}
