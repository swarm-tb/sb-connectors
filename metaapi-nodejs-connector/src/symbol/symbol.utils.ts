import moment from 'moment';
import { MetatraderSessions } from 'metaapi.cloud-sdk/dist/clients/metaApi/metaApiWebsocket.client';

const DAYS_OF_WEEK = {
  0: 'SUNDAY',
  1: 'MONDAY',
  2: 'TUESDAY',
  3: 'WEDNESDAY',
  4: 'THURSDAY',
  5: 'FRIDAY',
  6: 'SATURDAY',
};

const TRADE_SESSION_TIME_FORMAT = 'HH:mm:ss.SSS';

export const isTradingSession = (metatraderSessions: MetatraderSessions): boolean => {
  const currentTimestamp = moment();
  const dayOfWeek = currentTimestamp.day();
  const tradeSessions = metatraderSessions[DAYS_OF_WEEK[dayOfWeek]] ?? [];
  const currentTime = currentTimestamp.format(TRADE_SESSION_TIME_FORMAT);
  return tradeSessions.some((session: { from: string; to: string }) => {
    return session.from <= currentTime && currentTime <= session.to;
  });
};
