import { Inject, Injectable } from '@nestjs/common';
import { MetaApiSymbolInfo, TradeMode } from '@swarm-tb/grpc-common/metaapi/meta-api-symbol';
import { isTradingSession } from './symbol.utils';
import { ConnectionMap } from '../metaapi.provider';
import { MetatraderSymbolSpecification } from 'metaapi.cloud-sdk';
import { stringToEnumValue } from '../common.utils';

@Injectable()
export class SymbolService {
  constructor(
    @Inject('MetaApiStreamingConnectionMap')
    private connectionMap: ConnectionMap,
  ) {}

  async getSymbolsInfo(accountId: string): Promise<MetaApiSymbolInfo[]> {
    const connection = this.connectionMap[accountId];
    return connection.terminalState.specifications.map((spec) => SymbolService.toMetaApiSymbolInfo(spec));
  }

  private static toMetaApiSymbolInfo(metatraderSymbolInfo: MetatraderSymbolSpecification): MetaApiSymbolInfo {
    return {
      symbol: metatraderSymbolInfo.symbol,
      baseCurrency: metatraderSymbolInfo.baseCurrency,
      profitCurrency: metatraderSymbolInfo.profitCurrency,
      minVolume: metatraderSymbolInfo.minVolume,
      maxVolume: metatraderSymbolInfo.maxVolume,
      volumeStep: metatraderSymbolInfo.volumeStep,
      contractSize: metatraderSymbolInfo.contractSize,
      tradeMode: stringToEnumValue(metatraderSymbolInfo.tradeMode, TradeMode),
      path: metatraderSymbolInfo.path,
      tradeSession: isTradingSession(metatraderSymbolInfo.tradeSessions),
    };
  }
}
