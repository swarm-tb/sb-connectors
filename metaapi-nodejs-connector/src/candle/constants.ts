export const STRING_TO_MILLIS_MAP = {
  '5m': 5 * 60 * 1000,
  '15m': 15 * 60 * 1000,
  '1h': 60 * 60 * 1000,
};

export const MAX_CANDLES_LIMIT = 1000;
