import { Controller } from '@nestjs/common';
import {
  MetaApiCandle,
  MetaApiCandleServiceController,
  MetaApiCandleServiceControllerMethods,
  MetaApiCandlesRequest,
  MetaApiCandlesResponse,
  MetaApiLatestCandleRequest,
} from '@swarm-tb/grpc-common/metaapi/meta-api-candle';
import { CandleService } from './candle.service';

@Controller()
@MetaApiCandleServiceControllerMethods()
export class CandleController implements MetaApiCandleServiceController {
  constructor(private readonly candleService: CandleService) {}

  async getCandles(request: MetaApiCandlesRequest): Promise<MetaApiCandlesResponse> {
    return {
      candles: await this.candleService.getCandles(
        request.accountId,
        request.symbol,
        request.interval,
        request.startTime,
        request.endTime,
      ),
    };
  }

  async getLatestCandle(request: MetaApiLatestCandleRequest): Promise<MetaApiCandle> {
    return this.candleService.getLatestCandle(request.accountId, request.symbol, request.timeframe);
  }
}
