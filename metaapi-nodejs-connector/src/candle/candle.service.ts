import { Inject, Injectable } from '@nestjs/common';
import { MetatraderCandle, StreamingMetaApiConnectionInstance } from 'metaapi.cloud-sdk';
import { MetaApiCandle } from '@swarm-tb/grpc-common/metaapi/meta-api-candle';
import { ConnectionMap } from '../metaapi.provider';
import { MAX_CANDLES_LIMIT, STRING_TO_MILLIS_MAP } from './constants';

@Injectable()
export class CandleService {
  constructor(
    @Inject('MetaApiStreamingConnectionMap')
    private connectionMap: ConnectionMap,
  ) {}

  async getCandles(
    accountId: string,
    symbol: string,
    interval: string,
    startDate: string,
    endDate: string,
  ): Promise<MetaApiCandle[]> {
    console.log(`Downloading data for ${symbol}`);
    const connection = this.connectionMap[accountId];
    const intervalMillis = STRING_TO_MILLIS_MAP[interval];
    const startTime = new Date(startDate);
    let finalCandles: MetaApiCandle[] = [];
    let currentEndTime = new Date(endDate);

    while (true) {
      const timeDiff = currentEndTime.getTime() - startTime.getTime();
      const candlesNeeded = Math.floor(timeDiff / intervalMillis + 1);
      const limit = Math.min(candlesNeeded, MAX_CANDLES_LIMIT);

      const candles = await this.fetchCandles(connection, symbol, interval, currentEndTime.toISOString(), limit);

      const newCandles = candles.filter((candle) => new Date(candle.time) >= startTime);

      if (newCandles.length === 0) {
        break;
      }

      finalCandles = [...newCandles, ...finalCandles];

      currentEndTime = new Date(newCandles[0].time);
      if (currentEndTime <= startTime || newCandles.length < limit) {
        break;
      }
      currentEndTime.setMilliseconds(currentEndTime.getMilliseconds() - 1);
    }

    return finalCandles;
  }

  private async fetchCandles(
    connection: StreamingMetaApiConnectionInstance,
    symbol: string,
    interval: string,
    startTime: string,
    limit: number,
  ): Promise<MetaApiCandle[]> {
    const historicalCandles = await connection.account.getHistoricalCandles(
      symbol,
      interval,
      new Date(startTime),
      limit,
    );
    return historicalCandles.map((value) => CandleService.toMetaApiCandle(value));
  }

  async getLatestCandle(accountId: string, symbol: string, interval: string): Promise<MetaApiCandle> {
    const connection = this.connectionMap[accountId].account.getRPCConnection();
    await connection.connect();
    return CandleService.toMetaApiCandle(await connection.getCandle(symbol, interval, true));
  }

  private static toMetaApiCandle(metatraderCandle: MetatraderCandle): MetaApiCandle {
    return {
      time: metatraderCandle.time.toISOString(),
      open: metatraderCandle.open,
      high: metatraderCandle.high,
      low: metatraderCandle.low,
      close: metatraderCandle.close,
      volume: metatraderCandle.volume,
    };
  }
}
