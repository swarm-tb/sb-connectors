import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import { getGrpcOptions } from './grpc.options';

async function bootstrap() {
  // Reflection does not work in hybrid application mode (works only with createMicroservice method not connectMicroservice).
  // This is why ConfigService is instantiated here instead of getting it from AppModule.
  // TODO: Correct when the following is fixed https://github.com/nestjs/nest/issues/2343
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, getGrpcOptions(new ConfigService()));
  await app.listen();
}
bootstrap();
