import { ConfigService } from '@nestjs/config';
import { GrpcOptions, Transport } from '@nestjs/microservices';
import { COM_VIGIL_TRADING_INTEGRATION_METAAPI_PACKAGE_NAME } from '@swarm-tb/grpc-common/meta-api';
import { addReflectionToGrpcConfig, GrpcReflectionModule } from 'nestjs-grpc-reflection';

export function getGrpcOptions(configService: ConfigService): GrpcOptions {
  return addReflectionToGrpcConfig({
    transport: Transport.GRPC,
    options: {
      url: configService.get<string>('GRPC_URL'),
      package: COM_VIGIL_TRADING_INTEGRATION_METAAPI_PACKAGE_NAME,
      protoPath: require.resolve('@swarm-tb/grpc-common/proto/meta-api.proto'),
    },
  });
}

export const GrpcReflectionDynamicModule = GrpcReflectionModule.registerAsync({
  useFactory: async (configService: ConfigService) => {
    return getGrpcOptions(configService);
  },
  inject: [ConfigService],
});
