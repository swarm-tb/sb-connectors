#!/bin/bash

PROJECT_NAME="$1"
PROJECT_PATH="$CI_REGISTRY_IMAGE/$PROJECT_NAME"
CACHE_PATH="$PROJECT_PATH/paketo-build-cache"
VERSION=$(jq -r .version "$PROJECT_NAME/package.json")

/cnb/lifecycle/creator -app "$PROJECT_NAME" -cache-image "$CACHE_PATH:$VERSION" "$PROJECT_PATH:$VERSION"
