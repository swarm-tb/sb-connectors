plugins {
    id("com.vigil.trading.common-dependencies") version "1.0.1"
}

version = "1.0.1"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-json")
    implementation("org.springframework.cloud:spring-cloud-starter-openfeign")
}
