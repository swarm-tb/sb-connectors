package com.vigil.trading.snapi.utility

import java.time.Duration
import java.time.Instant

class ProgressBar(fromTimestamp: Instant, toTimestamp: Instant) {
    private var totalDuration: Duration = Duration.between(fromTimestamp, toTimestamp)

    fun printProgress(fromTimestamp: Instant, toTimestampTemp: Instant) {
        val progress = getProgress(fromTimestamp, toTimestampTemp)
        val progressBar = buildString {
            append("[")
            for (i in 0..<100) {
                if (i < progress * 100) {
                    append("=")
                } else {
                    append(" ")
                }
            }
            append("]")
        }
        print("\r$progressBar ${"%.2f".format(progress * 100)}%")
        System.out.flush() // Ensure that the output is flushed immediately
    }

    private fun getProgress(fromTimestamp: Instant, toTimestampTemp: Instant): Double {
        val remainingDuration = Duration.between(fromTimestamp, toTimestampTemp)
        return 1 - (remainingDuration.toMillis().toDouble() / totalDuration.toMillis().toDouble())
    }
}
