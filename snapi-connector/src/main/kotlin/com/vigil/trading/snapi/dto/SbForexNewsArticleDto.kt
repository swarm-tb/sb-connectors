package com.vigil.trading.snapi.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class SbForexNewsArticleDto(
    @JsonProperty("currency")
    override val symbols: List<String>
) : SbNewsArticleDto()
