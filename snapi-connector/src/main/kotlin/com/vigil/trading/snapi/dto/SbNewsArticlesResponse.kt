package com.vigil.trading.snapi.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class SbNewsArticlesResponse<T : SbNewsArticleDto>(
    val data: List<T>,
    @JsonProperty("total_pages")
    val totalPages: Int
)
