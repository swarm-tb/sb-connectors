package com.vigil.trading.snapi.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class SbCryptoNewsArticleDto(
    @JsonProperty("tickers")
    override val symbols: List<String>
) : SbNewsArticleDto()
