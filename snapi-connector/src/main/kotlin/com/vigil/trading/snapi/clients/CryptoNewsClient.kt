package com.vigil.trading.snapi.clients

import com.vigil.trading.snapi.dto.SbCryptoNewsArticleDto
import com.vigil.trading.snapi.dto.SbNewsArticlesResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(name = "cryptoNewsClient", url = "https://cryptonews-api.com/api/v1")
interface SnapiCryptoNewsClient : NewsClient {
    @GetMapping
    override fun getNews(
        @RequestParam(name = "tickers") symbols: String,
        @RequestParam dateTimeRange: String,
        @RequestParam items: Int,
        @RequestParam page: Int,
        @RequestParam token: String
    ): SbNewsArticlesResponse<SbCryptoNewsArticleDto>
}
