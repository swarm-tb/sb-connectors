package com.vigil.trading.snapi

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("snapi")
data class SnapiProperties(val cryptoApiToken: String, val forexApiToken: String)
