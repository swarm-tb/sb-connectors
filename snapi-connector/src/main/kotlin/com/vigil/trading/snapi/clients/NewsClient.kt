package com.vigil.trading.snapi.clients

import com.vigil.trading.snapi.dto.SbAccountUsageResponse
import com.vigil.trading.snapi.dto.SbNewsArticleDto
import com.vigil.trading.snapi.dto.SbNewsArticlesResponse
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

interface NewsClient {

    fun getNews(
        symbols: String,
        dateTimeRange: String,
        items: Int,
        page: Int,
        token: String
    ): SbNewsArticlesResponse<out SbNewsArticleDto>

    @GetMapping("/account/usage")
    fun getAccountUsage(@RequestParam token: String): SbAccountUsageResponse
}
