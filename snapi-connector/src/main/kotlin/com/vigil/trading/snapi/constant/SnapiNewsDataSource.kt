package com.vigil.trading.snapi.constant

enum class SnapiNewsDataSource {
    SNAPI_CRYPTO,
    SNAPI_FOREX,
    SNAPI_STOCKS
}
