package com.vigil.trading.snapi

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Configuration


@EnableFeignClients
@EnableConfigurationProperties(SnapiProperties::class)
@Configuration
open class SnapiConfig
