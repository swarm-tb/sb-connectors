package com.vigil.trading.snapi.clients

import com.vigil.trading.snapi.dto.SbForexNewsArticleDto
import com.vigil.trading.snapi.dto.SbNewsArticlesResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(name = "forexNewsClient", url = "https://forexnewsapi.com/api/v1")
interface SnapiForexNewsClient : NewsClient {
    @GetMapping
    override fun getNews(
        @RequestParam(name = "currencypair") symbols: String,
        @RequestParam dateTimeRange: String,
        @RequestParam items: Int,
        @RequestParam page: Int,
        @RequestParam token: String
    ): SbNewsArticlesResponse<SbForexNewsArticleDto>
}
