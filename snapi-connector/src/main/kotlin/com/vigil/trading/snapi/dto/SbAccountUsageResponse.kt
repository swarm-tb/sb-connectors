package com.vigil.trading.snapi.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class SbAccountUsageResponse(
    @JsonProperty("requests_left") val requestsLeft: Int
)
