package com.vigil.trading.snapi.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.vigil.trading.snapi.jackson.SnapiTimestampDeserializer
import java.time.Instant

abstract class SbNewsArticleDto {
    @JsonProperty("news_url")
    lateinit var newsUrl: String
    @JsonDeserialize(using = SnapiTimestampDeserializer::class)
    @JsonProperty("date")
    lateinit var timestamp: Instant
    lateinit var sentiment: String
    @JsonProperty("source_name")
    lateinit var sourceName: String
    abstract val symbols: List<String>
}
