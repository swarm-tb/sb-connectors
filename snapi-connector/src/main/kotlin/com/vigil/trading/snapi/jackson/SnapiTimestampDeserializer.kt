package com.vigil.trading.snapi.jackson

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*

class SnapiTimestampDeserializer : JsonDeserializer<Instant>() {
    private val formatter = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH)

    override fun deserialize(jsonParser: JsonParser, context: DeserializationContext): Instant {
        return Instant.from(formatter.parse(jsonParser.text))
    }
}
