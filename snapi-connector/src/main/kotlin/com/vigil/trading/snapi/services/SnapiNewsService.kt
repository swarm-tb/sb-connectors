package com.vigil.trading.snapi.services

import com.vigil.trading.snapi.SnapiProperties
import com.vigil.trading.snapi.clients.SnapiCryptoNewsClient
import com.vigil.trading.snapi.clients.SnapiForexNewsClient
import com.vigil.trading.snapi.constant.SnapiNewsDataSource
import com.vigil.trading.snapi.dto.SbNewsArticleDto
import com.vigil.trading.snapi.logger
import com.vigil.trading.snapi.utility.ProgressBar
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

@Service
class SnapiNewsService(
    snapiCryptoNewsClient: SnapiCryptoNewsClient,
    snapiForexNewsClient: SnapiForexNewsClient,
    snapiProperties: SnapiProperties
) {
    private val clientsMap = mapOf(
        SnapiNewsDataSource.SNAPI_CRYPTO.name to (snapiCryptoNewsClient to snapiProperties.cryptoApiToken),
        SnapiNewsDataSource.SNAPI_FOREX.name to (snapiForexNewsClient to snapiProperties.forexApiToken)
    )
    private val snapiNewsTimezone = ZoneId.of("UTC-4")
    private val dateFormatter = DateTimeFormatter.ofPattern("MMddyyyy").withZone(snapiNewsTimezone)
    private val timeFormatter = DateTimeFormatter.ofPattern("HHmmss").withZone(snapiNewsTimezone)

    fun getNewsForSymbols(
        newsDataSource: String, tickers: List<String>, fromTimestamp: Instant, toTimestamp: Instant
    ): List<SbNewsArticleDto> {
        val (newsClient, token) = clientsMap[newsDataSource]
            ?: throw IllegalArgumentException("News data source $newsDataSource is not supported.")
        var pageNumber = 1
        val allNews = mutableListOf<SbNewsArticleDto>()
        val requestsLeft = newsClient.getAccountUsage(token).requestsLeft
        var toTimestampTemp = toTimestamp
        log.info("Requests left: $requestsLeft")
        val progressBar = ProgressBar(fromTimestamp, toTimestamp)
        while (true) {
            val newsForTickers = newsClient.getNews(
                prepareTickers(tickers),
                prepareDateTimeRange(fromTimestamp, toTimestampTemp),
                MAX_ITEMS_COUNT,
                pageNumber,
                token
            )
            if (requestsLeft < newsForTickers.totalPages) {
                throw RuntimeException("Not enough requests left. $requestsLeft left, ${newsForTickers.totalPages} needed.")
            }
            allNews.addAll(newsForTickers.data)
            val lastTimestamp = newsForTickers.data[newsForTickers.data.size - 1].timestamp
            progressBar.printProgress(fromTimestamp, lastTimestamp)
            if (pageNumber < newsForTickers.totalPages) {
                pageNumber++
            } else {
                if (newsForTickers.totalPages < MAX_PAGES_COUNT) {
                    break
                }
                pageNumber = 1
                toTimestampTemp = lastTimestamp
            }
        }
        return allNews
    }

    private fun prepareDateTimeRange(fromTimestamp: Instant, toTimestamp: Instant): String {
        val startDateTime = "${dateFormatter.format(fromTimestamp)}+${timeFormatter.format(fromTimestamp)}"
        val endDateTime = "${dateFormatter.format(toTimestamp)}+${timeFormatter.format(toTimestamp)}"
        return "$startDateTime-$endDateTime"
    }

    private fun prepareTickers(tickers: List<String>): String {
        return tickers.joinToString(",")
    }

    companion object {
        private val log by logger()
        private const val MAX_ITEMS_COUNT = 100
        private const val MAX_PAGES_COUNT = MAX_ITEMS_COUNT
    }
}
